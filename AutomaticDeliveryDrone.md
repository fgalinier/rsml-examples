# Automatic Delivery Drone Example

## Description

The aim of the automatic delivery drone (later called \"the drone\") is to transport parcels from the commercial enterprise warehouse to its customers. The drone is a quadricopter equipped with a clamp. 
When activated, the drone shall take over a parcel (pick it in the warehouse and deliver it). 
The drone shall be controlled by a web application, and being accessible all the time by this application. 
In normal mode, the drone shall be fully automated, but an operator must be able to switch to control mode to take control of the drone with the application. The drone is propelled by four rotors, each driven by a brushless motor. The power shall be enough to transport parcels weighing up to 10 kg and with a size up to 1x1 m.

## How shall it works?

The drone shall pick up a parcel, go to the destination and drop it off when activated on the web application.
An operator shall assigned a parcel (identified by a unique identifier -- encoded in a QR code on the parcel -- and a position in the warehouse) to a drone through the application. 
The drone shall go to the parcel, catch it, and deliver it to the destination (collected from a service that give a destination (GPS coordinates) given an identifier).
When the destination is reached, the drone shall drop the parcel and return back to the warehouse to its charge station.

## Requirements

### Handling requirements

1. When a parcel is assigned to the drone, the drone shall go to the parcel and catch it. 
	1. When the parcel is not at the given position in warehouse, the drone shall inform an operator. 
		1. If there is no parcel in the given position in warehouse, the drone shall send a message to an operator.
		2. If the parcel in the given position in warehouse is not the good one, the drone shall send a message to an operator.
		3. When the parcel is not at the given position in warehouse, the drone shall send a message to the web application.
		4. When the parcel is not at the given position in warehouse, the drone must emit an audible warning signal and shall remain on standby.
	2. When the battery is under 10%, the drone shall stay in charge and a notification of battery status shall be available for the user.
		1. When delivery order is given and when the battery is low (under 10%), the drone shall send a notification and stay in the charge pad.

### Transport requirements

2. When a parcel is handled by the drone, the drone shall transport it to its destination.
	1. The drone shall retrieve destination of handled parcel from an existing webservice and transport it to this destination.
3. When the drone handle a parcel, then it shall transport it carefully.
	1. At any time, the drone shall avoid obstacles, fly at the minimum safe height and be able to stop in less than 1 second.
		1. The drone shall avoid obstacle.
	2. The drone shall drop the parcel carefully.
		1. The distance between the ground and the parcel shall be less than 10 cm.
	3. If one or several components are malfunctioning, the drone shall send a message to an operator.
	4. When its battery is low then the drone shall send a message and shall land.
		1. When its battery is low then the drone shall send a message.
		2. When its battery is low then the drone shall land.
	5. When the distance to warehouse is more than 500 meters and when the drone battery is under 10%, then the drone shall switch to recovery mode, send a location message and land quickly.
		1. When the distance to warehouse is more than 500 meters then the drone shall send a position message.
		2. When the battery is under 10% then the drone shall switch to recovery mode and land quickly.

### Drop requirements

4. The drone shall drop parcel at destination.
	1. An operator shall confirm the parcel drop location.

### Return requirements

5. When the drone has no more parcel assigned and attached, the drone shall go back to its charge station.

### Performance requirements

6. The drone shall be efficient.
	1. The drone shall be able to fly during 1 hour.
	2. The drone shall be able to fly inside and outside.