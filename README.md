Here are RSML representation of requirements (and the Eiffel semantics) for LGS and LAS case studies.
Specification used to verify these requirements can be found:
- For the LGS, [https://github.com/anaumchev/lgs_ground_model](here);
- For the LAS, [https://gitlab.com/fgalinier/LAS/blob/master/las.e](here).
