note
	EIS: "src=LAS.rsml", "ref=LAS", "type=trace"
	description: "[
	This class contains requirements in the context of: LAS.
]"
class LAS_REQUIREMENTS
feature
	-- For temporal requirements
	duration: DOUBLE
	
	-- States range
	intervened : DOUBLE = 1
	mobilized : DOUBLE = 2
	free : DOUBLE = 3
	allocated : DOUBLE = 4
	encoded : DOUBLE = 5
	
	-- States
	an_emergency : DOUBLE
	intervention_status : DOUBLE
	availability_state : DOUBLE
	mobilization_status : DOUBLE
	ambulance_state : DOUBLE

feature -- Requirements

	when_an_emergency_is_encoded_then_eventually_ambulance_state_must_be_allocated
	note
		EIS: "src=LAS.rsml", "ref=[1]", "type=trace"
		Description: "[
			[1] When an emergency is equal to encoded then eventually ambulance state must be equal to allocated within 120 [s].
		]"
	do
		from
			check assume: (an_emergency = encoded) end
			-- Add call to specification
		until
			((ambulance_state = allocated)) or
			(duration - old duration) > 120
		loop
			check assume: (an_emergency = encoded) end
			-- Add call to specification
		end
		check assert: (ambulance_state = allocated) end
		check assert: (duration - old duration) <= 120 end
	end
	
	when_ambulance_state_is_allocated_then_eventually_mobilization_status_must_be_mobilized
	note
		EIS: "src=LAS.rsml", "ref=[2]", "type=trace"
		Description: "[
			[2] When ambulance state is equal to allocated then eventually mobilization status must be equal to mobilized within 120 [s].
		]"
	do
		from
			check assume: (ambulance_state = allocated) end
			-- Add call to specification
		until
			((mobilization_status = mobilized)) or
			(duration - old duration) > 120
		loop
			check assume: (ambulance_state = allocated) end
			-- Add call to specification
		end
		check assert: (mobilization_status = mobilized) end
		check assert: (duration - old duration) <= 120 end
	end
	
	when_ambulance_state_is_allocated_then_immediately_availability_state_should_not_be_free
	note
		EIS: "src=LAS.rsml", "ref=[3]", "type=trace"
		Description: "[
			[3] When ambulance state is equal to allocated then immediately availability state shall be not equal to free.
		]"
	require
		when_ambulance_state_is_equal_to_allocated: (ambulance_state = allocated)
	deferred
	ensure
		check_availability_state_shall_be_not_equal_to_free: (availability_state /= free)
	end
	
	when_ambulance_state_is_not_allocated_then_immediately_mobilization_status_should_not_be_mobilized
	note
		EIS: "src=LAS.rsml", "ref=[4]", "type=trace"
		Description: "[
			[4] When ambulance state is not equal to allocated then immediately mobilization status shall be not equal to mobilized.
		]"
	require
		when_ambulance_state_is_not_equal_to_allocated: (ambulance_state /= allocated)
	deferred
	ensure
		check_mobilization_status_shall_be_not_equal_to_mobilized: (mobilization_status /= mobilized)
	end
	
	when_ambulance_state_is_allocatedintervention_status_is_not_intervened_then_immediately_mobilization_status_should_be_mobilized
	note
		EIS: "src=LAS.rsml", "ref=[5]", "type=trace"
		Description: "[
			[5] When ambulance state is equal to allocated and intervention status is not equal to intervened then immediately mobilization status shall be equal to mobilized.
		]"
	require
		when_ambulance_state_is_equal_to_allocated: (ambulance_state = allocated)
		when_intervention_status_is_not_equal_to_intervened: (intervention_status /= intervened)
	deferred
	ensure
		check_mobilization_status_shall_be_equal_to_mobilized: (mobilization_status = mobilized)
	end
	
end
