note
	EIS: "src=LGS.rsml", "ref=LGS", "type=trace"
	description: "[
	This class contains requirements in the context of: LGS.
]"
class LGS_REQUIREMENTS
feature
	retracted : DOUBLE
	retracting : DOUBLE
	doors_status : DOUBLE
	closed : DOUBLE
	handle_position : DOUBLE
	up : DOUBLE
	landing_gear : DOUBLE
	down : DOUBLE
	extended : DOUBLE
	extending : DOUBLE

feature -- Requirements

	when_handle_position_is_down_then_eventually_landing_gear_should_be_extendeddoors_status_should_be_closed
	note
		EIS: "src=LGS.rsml", "ref=[11]", "type=trace"
		Description: "[
			[11] When the handle position is equal to down then eventually the landing gear shall be equal to extended and the doors status shall be equal to closed within 12 [seconds].
		]"
	do
		from
			check assume: (handle_position = down) end
			-- Add call to specification
		until
			((landing_gear = extended) and (doors_status = closed)) or
			(duration - old duration) > 12
		loop
			check assume: (handle_position = down) end
			-- Add call to specification
		end
		check assert: (landing_gear = extended) end
		check assert: (doors_status = closed) end
		check assert: (duration - old duration) <= 12 end
	end
	
	when_handle_position_is_up_then_eventually_landing_gear_should_be_retracteddoors_status_should_be_closed
	note
		EIS: "src=LGS.rsml", "ref=[12]", "type=trace"
		Description: "[
			[12] When the handle position is equal to up then eventually the landing gear shall be equal to retracted and the doors status shall be equal to closed  within 30 [seconds].
		]"
	do
		from
			check assume: (handle_position = up) end
			-- Add call to specification
		until
			((landing_gear = retracted) and (doors_status = closed)) or
			(duration - old duration) > 30
		loop
			check assume: (handle_position = up) end
			-- Add call to specification
		end
		check assert: (landing_gear = retracted) end
		check assert: (doors_status = closed) end
		check assert: (duration - old duration) <= 30 end
	end
	
	when_handle_position_is_down_then_immediately_landing_gear_must_not_be_retracting
	note
		EIS: "src=LGS.rsml", "ref=[21]", "type=trace"
		Description: "[
			[21] When the handle position is equal to down then immediately the landing gear must be not equal to retracting.
		]"
	require
		when_handle_position_is_equal_to_down: (handle_position = down)
	deferred
	ensure
		check_landing_gear_must_be_not_equal_to_retracting: (landing_gear /= retracting)
	end
	
	when_handle_position_is_up_then_immediately_landing_gear_must_not_be_extending
	note
		EIS: "src=LGS.rsml", "ref=[22]", "type=trace"
		Description: "[
			[22] When the handle position is equal to up then immediately the landing gear must be not equal to extending.
		]"
	require
		when_handle_position_is_equal_to_up: (handle_position = up)
	deferred
	ensure
		check_landing_gear_must_be_not_equal_to_extending: (landing_gear /= extending)
	end
	
end
